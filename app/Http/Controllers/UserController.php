<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

class UserController extends Controller
{
//    enlista users
    public function index()
    {
        $users = User::orderBy('id', 'desc')->paginate(5);
        return response()->json([
            "success" => true,
            "message" => "User List",
            "data" => $users
        ]);
    }
//    create user
    public function store(Request $request)
    {
    // primero se valida los datos que ingresan
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],

        ]);
    //se ponen en la varibale input
        $input = $request->all();
    //se create el user y se guarda en $user
        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'status' => 1,
            'password' => Hash::make($input['password']),

            ]);
    //se retorna en manera json
        return response()->json([
            "success" => true,
            "message" => "User created successfully.",
            "data" => $user
        ]);
    }

    public function show($id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return $this->sendError('User not found.');
        }
        return response()->json([
            "success" => true,
            "message" => "User retrieved successfully.",
            "data" => $user
        ]);
    }

    public function update(Request $request, User $user, $id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return $this->sendError('User not found.');
        }

        $request->validate([
            'name' => ['string', 'max:255'],
            'email' => ['string', 'email', 'max:255', 'unique:users'],
            'password' => ['string', 'min:8'],

        ]);

        $input = $request->all();

        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->password = Hash::make($input['password']);
        $user->save();

        return response()->json([
            "success" => true,
            "message" => "User updated successfully.",
            "data" => $user
        ]);
    }

    public function destroy(User $user, $id)
    {
        $user = User::find($id);

        $user->status = 0;

        $user->save();

        return response()->json([
            "success" => true,
            "message" => "User deleted successfully.",
            "data" => $user
        ]);
    }

}
