<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//API ROUTES
Route::post('/auth/register', [App\Http\Controllers\Auth\AuthController::class, 'register'])->name('register');
Route::post('/auth/login', [App\Http\Controllers\Auth\AuthController::class, 'login'])->name('login');

//forget
Route::post('/auth/forget/password', [App\Http\Controllers\Auth\AuthController::class, 'store'])->name('store');

//reset pass
Route::get('/auth/reset/password/{token}', [App\Http\Controllers\Auth\AuthController::class, 'getPassword'])->name('getPassword');
Route::post('/auth/reset/password', [App\Http\Controllers\Auth\AuthController::class, 'updatePassword'])->name('updatePassword');

//user routes
Route::get('/users', [App\Http\Controllers\UserController::class, 'index']);
Route::post('/create/users', [App\Http\Controllers\UserController::class, 'store']);
Route::get('/users/{id}', [App\Http\Controllers\UserController::class, 'show']);
Route::post('/users/{id}', [App\Http\Controllers\UserController::class, 'update']);
Route::post('/delete/users/{id}', [App\Http\Controllers\UserController::class, 'destroy']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});












